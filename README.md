# panacea

panacea is an [elixire] uploader written in Rust.

[elixire]: https://gitlab.com/elixire/elixire

```sh
# install; make sure you have rustc + cargo, from rustup:
$ cargo install --git https://gitlab.com/elixire/panacea.git

# then, run the command to get started:
$ panacea

# now, upload a file; the url is printed to stdout:
$ panacea file.png

# you may also upload from stdin:
$ panacea < file.png
$ cat file.png | panacea
```

Library usage:

```rust
const TOKEN: &str = "...";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::builder(TOKEN.to_owned()).build()?;

    let upload = client.upload("hey there").await?;
    println!("{:?}", upload);

    Ok(())
}
```
