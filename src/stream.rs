// This code is adapted from https://github.com/SergioBenitez/Rocket.
// Permalink: https://github.com/SergioBenitez/Rocket/blob/a0cb52d/core/lib/src/ext.rs
// See: https://github.com/SergioBenitez/Rocket/blob/async/LICENSE-MIT

use bytes::Bytes;
use tokio::prelude::*;
use tokio::stream::Stream;

use std::pin::Pin;
use std::task::{Context, Poll};

/// A wrapper that turns something that implements `AsyncRead` into a `Stream`.
/// This is done by buffering reads.
pub struct BytesStream<R> {
    inner: R,
    buf_size: usize,
    buffer: Vec<u8>,
}

impl<R> BytesStream<R>
where
    R: AsyncRead + Unpin,
{
    pub fn wrap(inner: R, buf_size: usize) -> Self {
        Self {
            inner,
            buf_size,
            buffer: vec![0; buf_size],
        }
    }
}

impl<R> Stream for BytesStream<R>
where
    R: AsyncRead + Unpin,
{
    type Item = Result<Bytes, io::Error>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        debug_assert!(self.buffer.len() == self.buf_size);

        let Self {
            ref mut inner,
            ref mut buffer,
            buf_size,
        } = *self;

        match Pin::new(inner).poll_read(cx, &mut buffer[..]) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(Err(e)) => Poll::Ready(Some(Err(e))),
            Poll::Ready(Ok(n)) if n == 0 => Poll::Ready(None),
            Poll::Ready(Ok(n)) => {
                let mut next = std::mem::replace(buffer, vec![0; buf_size]);
                next.truncate(n);
                Poll::Ready(Some(Ok(Bytes::from(next))))
            }
        }
    }
}
