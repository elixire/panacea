//! Asynchronous [elixire] file uploader.
//!
//! [elixire]: https://gitlab.com/elixire/elixire
//!
//! ```no_run
//! use panacea::Client;
//!
//! #[tokio::main]
//! async fn main() -> Result<(), Box<dyn std::error::Error>> {
//!     let token = std::env::var("ELIXIRE_TOKEN")?;
//!     let client = Client::builder(token).build()?;
//!     let upload = client.upload("hello, panacea!").await?;
//!     println!("Uploaded some text to: {}", upload.url);
//!     Ok(())
//! }
//! ```
//!
//! Create and configure a [`Client`] using [`Client::builder`] or
//! [`ClientBuilder::new`]. Then, upload files and images using [`Client::upload`].
//! That method returns [`Upload`] structs. Possible errors are modeled in
//! [`UploadError`].
//!
//! Types that be converted [`Into`] [`reqwest::Body`] can be [`upload`](Client::upload)ed.
//!
//! # Asynchronous runtime
//!
//! panacea uses the [Reqwest] library to make HTTP requests. It depends on
//! [Tokio] to handle asynchronous I/O, which means that panacea must depend on
//! Tokio as well. Efforts to make panacea runtime-agnostic may be investigated
//! in the future.
//!
//! [tokio]: https://tokio.rs
//! [reqwest]: https://github.com/seanmonstar/reqwest
//!
//! # Blocking usage
//!
//! To use panacea in a synchronous context, you must manually construct a Tokio
//! runtime and call `Runtime::block_on`.
//!
//! ```no_run
//! use tokio::runtime::Runtime;
//!
//! async fn upload() -> Result<(), Box<dyn std::error::Error>> {
//!     let client = Client::builder("...".to_owned()).build()?;
//!     let upload = client.upload("hello, panacea!").await?;
//!     println!("Uploaded some text to: {}", upload.url);
//!     Ok(())
//! }
//!
//! fn main() -> Result<(), Box<dyn std::error::Error>> {
//!     let mut rt = Runtime::new()?;
//!     rt.block_on(upload())?;
//!     Ok(())
//! }
//! ```

pub mod builder;
pub mod client;
pub mod upload;
pub mod stream;

pub use builder::ClientBuilder;
pub use client::Client;
pub use upload::{Upload, UploadError};

/// An alias to [`Result`](std::result::Result), but with `E` fixed to [`UploadError`].
type Result<T> = std::result::Result<T, UploadError>;
