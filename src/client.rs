use crate::upload::{Upload, UploadError};
use crate::ClientBuilder;

use url::Url;

const USER_AGENT: &str = "panacea (https://gitlab.com/elixire/panacea)";

/// A helper that uploads files to an [elixire] v2 instance.
///
/// One can be created with the [`ClientBuilder`]. You can make a builder with
/// [`Client::builder`] or [`ClientBuilder::new`].
///
/// [elixire]: https://gitlab.com/elixire/elixire
pub struct Client {
    token: String,
    admin: bool,
    base_url: Url,
    client: reqwest::Client,
}

impl Client {
    /// Creates a [`ClientBuilder`]. This is the same as calling [`ClientBuilder::new`].
    pub fn builder(token: String) -> ClientBuilder {
        crate::ClientBuilder::new(token)
    }

    pub(crate) fn new(base_url: Url, token: String, admin: bool) -> reqwest::Result<Self> {
        use reqwest::{
            header::{HeaderMap, HeaderValue},
            ClientBuilder,
        };

        let mut headers = HeaderMap::new();
        headers.insert("User-Agent", HeaderValue::from_static(USER_AGENT));

        let client = ClientBuilder::new().default_headers(headers).build()?;

        Ok(Self {
            token,
            admin,
            base_url,
            client,
        })
    }

    async fn upload_bytes(&self, body: reqwest::Body) -> crate::Result<Upload> {
        use reqwest::multipart::{Form, Part};
        use reqwest::StatusCode;

        let form = Form::new().part(
            "file",
            Part::stream(body)
                .mime_str("application/octet-stream")
                .unwrap()
                .file_name("file"),
        );

        let mut upload_endpoint = self.base_url.join("/api/upload").unwrap();

        if self.admin {
            upload_endpoint.set_query(Some("admin=1"));
        }

        let request = self
            .client
            .post(upload_endpoint)
            .header("Authorization", &self.token)
            .multipart(form);

        let response = request.send().await.map_err(UploadError::RequestError)?;

        let quota_exceeded_status_code = StatusCode::from_u16(469).unwrap();
        match response.status() {
            status if status == quota_exceeded_status_code => {
                return Err(UploadError::QuotaExceeded);
            }
            StatusCode::UNSUPPORTED_MEDIA_TYPE => {
                return Err(UploadError::UnsupportedFileType);
            }
            StatusCode::OK => {}
            _ => return Err(UploadError::ServerError(Box::new(response))),
        };

        let upload = response
            .json::<Upload>()
            .await
            .map_err(UploadError::DecodeError)?;

        Ok(upload)
    }

    /// Uploads something. You can upload anything that can be converted into
    /// [`reqwest::Body`].
    ///
    /// ```no_run
    /// let client = Client::builder("...").admin(true).build();
    /// let upload = client.upload("just uploading a string").await?;
    /// println!("Uploaded: {}", upload.url);
    /// ```
    pub async fn upload(&self, body: impl Into<reqwest::Body>) -> crate::Result<Upload> {
        self.upload_bytes(body.into()).await
    }
}
