use panacea::stream::BytesStream;
use panacea::{Client, Upload, UploadError};

use serde::Deserialize;

use std::path::PathBuf;

/// The amount of bytes to read at once when streaming bytes from stdin or a file.
const STREAM_BUF_SIZE: usize = 8192;

#[derive(Deserialize)]
struct Config {
    token: String,
    admin: Option<bool>,
    base_url: Option<String>,
}

enum CliError {
    NoSystemConfigDir,
    NoConfigFile(PathBuf),
    CantReadConfig(PathBuf, tokio::io::Error),
    CantOpenTargetFile(PathBuf, tokio::io::Error),
    CantDeserializeConfig(PathBuf, toml::de::Error),
    CantCreateClient(reqwest::Error),
    CantUpload(UploadError),
    NoTarget,
    InvalidBaseUrl(String, url::ParseError),
    CantSetBaseUrl(String, reqwest::Error),
}

fn config_toml_path() -> Option<PathBuf> {
    dirs::config_dir().map(|c| c.join("panacea").join("config.toml"))
}

async fn read_config(path: Option<PathBuf>) -> Result<Config, CliError> {
    let config_path = path
        .or_else(|| config_toml_path())
        .ok_or(CliError::NoSystemConfigDir)?;

    // Check for the file's existence.
    if tokio::fs::metadata(&config_path).await.is_err() {
        return Err(CliError::NoConfigFile(config_path));
    }

    let config_text = tokio::fs::read_to_string(&config_path)
        .await
        .map_err(|err| CliError::CantReadConfig(config_path.clone(), err))?;

    let config: Config = toml::from_str(&config_text)
        .map_err(|err| CliError::CantDeserializeConfig(config_path.clone(), err))?;

    Ok(config)
}

async fn upload(config: &Config, file: Option<PathBuf>) -> Result<Upload, CliError> {
    let mut builder = Client::builder(config.token.clone()).admin(config.admin.unwrap_or(false));

    if let Some(custom_base_url) = &config.base_url {
        use url::Url;
        let url = Url::parse(custom_base_url)
            .map_err(|err| CliError::InvalidBaseUrl(custom_base_url.clone(), err))?;
        builder = builder
            .base_url(url)
            .map_err(|err| CliError::CantSetBaseUrl(custom_base_url.clone(), err))?;
    }

    let client = builder.build().map_err(CliError::CantCreateClient)?;

    let upload_res: Result<Upload, UploadError>;

    if let Some(path) = file {
        // Always use a specified file.

        let file = tokio::fs::File::open(&path)
            .await
            .map_err(|err| CliError::CantOpenTargetFile(path.clone(), err))?;
        let stream = BytesStream::wrap(file, STREAM_BUF_SIZE);
        let body = reqwest::Body::wrap_stream(stream);

        upload_res = client.upload(body).await;
    } else {
        // If no file was specified, use stdin.
        // If stdin is a TTY, error out.
        if atty::is(atty::Stream::Stdin) {
            return Err(CliError::NoTarget);
        }

        let stream = BytesStream::wrap(tokio::io::stdin(), STREAM_BUF_SIZE);
        let body = reqwest::Body::wrap_stream(stream);

        upload_res = client.upload(body).await;
    }

    let upload = upload_res.map_err(CliError::CantUpload)?;

    Ok(upload)
}

fn bail(message: impl AsRef<str>) -> ! {
    eprintln!("{}", message.as_ref());
    std::process::exit(1);
}

fn handle_cli_err(err: CliError) -> ! {
    use CliError::*;

    match err {
        NoSystemConfigDir => {
            bail(
                "Unable to locate your system configuration directory. \
                Are you on an obscure platform?",
            );
        }
        NoConfigFile(path) => {
            let path = path.display();
            let editor = std::env::var("EDITOR");
            let edit_remark = match editor {
                Ok(editor) => format!("Run:\n\n\t{} {}\n\nto create one.", editor, path),
                _ => format!("Edit this file:\n\n\t{}\n\nto create one.", path),
            };
            bail(format!(
                "You haven't created a config file yet! {} \
                Here's what the minimum config looks like:\n\n\ttoken = \"...\"\n\n\
                If you don't have an uploader token yet, you can generate one at https://elixi.re/uploader.html.",
                edit_remark,
            ));
        }
        CantReadConfig(path, err) => bail(format!(
            "Unable to read your config file ({}): {}",
            path.as_path().display(),
            err
        )),
        CantDeserializeConfig(path, err) => bail(format!(
            "Unable to deserialize your config file ({}). Did you make any typos?\n\n\t{}",
            path.display(),
            err
        )),
        CantCreateClient(err) => bail(format!("Unable to create an HTTP client: {}.", err)),
        CantUpload(UploadError::UnsupportedFileType) => bail("You can't upload that kind of file."),
        CantUpload(UploadError::QuotaExceeded) => bail("Your upload quota has been exceeded!"),
        CantUpload(UploadError::DecodeError(err)) => bail(format!(
            "Unable to upload; the server returned something I didn't understand: {}.",
            err
        )),
        CantUpload(UploadError::ServerError(resp)) if resp.status() == reqwest::StatusCode::FORBIDDEN => bail(format!(
            "Unable to upload; token is invalid. Make sure you have entered your token correctly."
        )),
        CantUpload(UploadError::ServerError(resp)) => bail(format!(
            "Unable to upload; the server returned HTTP {}.",
            resp.status()
        )),
        CantUpload(UploadError::RequestError(err)) => bail(format!(
            "Unable to make the request to the server: {}. Are you online?",
            err
        )),
        CantOpenTargetFile(path, err) => bail(format!("Unable to upload; can't read the target file ({}): {}", path.display(), err)),
        InvalidBaseUrl(base_url, err) => bail(format!("Unable to parse custom base URL ({}): {}", base_url, err)),
        CantSetBaseUrl(base_url, err) => bail(format!("Unable to set base URL to {}: {}", base_url, err)),
        NoTarget => bail("No input given to upload and input is a TTY (terminal). You can pipe in bytes through stdin or pass a filename. For help, run with `--help`."),
    }
}

async fn run(matches: clap::ArgMatches<'_>) -> Result<(), CliError> {
    let config_path = matches.value_of("config_path").map(PathBuf::from);
    let target_path = matches.value_of("input").map(PathBuf::from);
    let config = read_config(config_path).await?;
    let upload = upload(&config, target_path).await?;
    println!("{}", upload.url);
    Ok(())
}

#[tokio::main]
async fn main() {
    use clap::{clap_app, crate_authors, crate_version};

    let matches = clap_app!(panacea =>
        (version: crate_version!())
        (author: crate_authors!())
        (about: "elixire file uploader")
        (after_help: "If an input file isn't given and stdin isn't a TTY (terminal) \
            then stdin will be uploaded. When uploading, any input is streamed \
            to the server by performing buffered reads. Thus, any upload can be \
            done in constant memory.")
        (@arg config_path: -c --config [CONFIG_PATH] +takes_value "Sets the config file to use")
        (@arg input: [FILE] "The file to upload")
    )
    .get_matches();

    if let Err(err) = run(matches).await {
        handle_cli_err(err);
    }
}
