use serde::Deserialize;

/// An uploaded file on elixire.
#[derive(Deserialize, Debug, Clone)]
pub struct Upload {
    /// The URL of the upload.
    pub url: String,

    /// The shortname of the upload.
    pub shortname: String,
}

#[derive(Debug)]
pub enum UploadError {
    /// The file type is unsupported.
    UnsupportedFileType,

    /// Your upload quota has been exceeded.
    QuotaExceeded,

    /// The JSON returned by the server was unable to be deserialized.
    DecodeError(reqwest::Error),

    /// An unknown error was returned from the server.
    ServerError(Box<reqwest::Response>),

    /// There was an error sending the request itself, e.g. connectivity problems.
    RequestError(reqwest::Error),
}

impl std::fmt::Display for UploadError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use UploadError::*;

        match self {
            UnsupportedFileType => write!(f, "unsupported file type"),
            QuotaExceeded => write!(f, "quota exceeded"),
            DecodeError(err) => write!(f, "failed to decode json: {}", err),
            ServerError(resp) => write!(f, "server returned {}", resp.status()),
            RequestError(err) => write!(f, "failed to send request: {}", err),
        }
    }
}

impl std::error::Error for UploadError {}
