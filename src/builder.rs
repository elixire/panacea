use crate::Client;

use url::Url;

/// A builder for a [`Client`][crate::Client]. It's used to configure the
/// `Client` pre-instantiation.
///
/// You must provide your uploader token upfront.
///
/// # Examples
///
/// ```
/// let client: Client = ClientBuilder::new("...")
///     .admin(true)
///     .base_url("https://my.elixire.instance")
///     .build();
/// ```
pub struct ClientBuilder {
    token: String,
    admin: bool,
    base_url: Url,
}

impl ClientBuilder {
    /// Creates a new `ClientBuilder` using the specified token.
    pub fn new(token: String) -> Self {
        Self {
            token,
            admin: false,
            base_url: Url::parse("https://elixi.re").unwrap(),
        }
    }

    /// Sets whether admin mode (`?admin=1`) should be used when uploading.
    ///
    /// Only usable by admins, this will skip all checks (e.g. MIME type checks
    /// and virus scanning).
    pub fn admin(mut self, admin: bool) -> Self {
        self.admin = admin;
        self
    }

    /// Sets the base instance URL to use when uploading.
    ///
    /// # Examples
    ///
    /// ```
    /// // `https://my.elixire.instance/api/upload` will be requested when
    /// // uploading.
    ///
    /// let client: Client = ClientBuilder::new("...")
    ///     .base_url("https://my.elixire.instance")
    ///     .build();
    /// ```
    pub fn base_url<U: reqwest::IntoUrl>(mut self, base_url: U) -> reqwest::Result<Self> {
        self.base_url = base_url.into_url()?;
        Ok(self)
    }

    /// Builds the [`Client`], which can be used to [upload][Client::upload].
    pub fn build(self) -> reqwest::Result<Client> {
        Client::new(self.base_url, self.token, self.admin)
    }
}
